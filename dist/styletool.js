(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([
          'inheritance',
          'dustmote/util',
          'dustmote/easing'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(
          require('inheritance'),
          require('dustmote/util'),
          require('dustmote/easing'));
    } else {
        // Browser globals (root is window)
        root.Dustmote.StyleTool = factory(
          root.Class,
          root.Dustmote.Util,
          root.Dustmote.Easing);
    }
}(this, function (Class, Util, Easing) {

  //----------------------------------------------------------------------------------------
  // Base
  //----------------------------------------------------------------------------------------
  var Base = Class.extend({
    initialize: function(start, end, easing) {
      if (Util.isFunction(end)) {
        easing  = easing || end;
        end     = start;
      }

      this.start    = start;
      this.end      = end;
      this.easing   = easing || Easing.easeLinear;
      this.scale;
      this.diff;

      if (Util.isArray(start) || Util.isArray(end)) {
        var self = this;
        this.diff = [];
        Util.interleave(start, end, function(a, b) {
          self.diff.push(a - b);
        });
      } else {
        this.diff = start - end;
      }
    },

    getScale: function(mote) {
      return this.easing(mote.age/mote.life);
    },

    perform: function(mote, shape, emitter_position) {
      this.scale = this.getScale(mote);
    },

    invertEveryN: function(n) {
      var perform   = this.perform;
      var diff1     = Util.isArray(this.diff)   ? this.diff.slice(0) : this.diff;
      var diff2     = Util.isArray(diff1)       ? diff1.map(function(a) { return a*-1; }) : diff1*-1;
      var start1    = Util.isArray(this.start)  ? this.start.slice(0) : this.start;
      var start2    = Util.isArray(this.end)    ? this.end.slice(0)   : this.end;
      var end1      = Util.isArray(this.end)    ? this.end.slice(0)   : this.end;
      var end2      = Util.isArray(this.start)  ? this.start.slice(0) : this.start;

      this.perform = function(mote, shape, emitter_position) {
        if (mote.id % n) {
          this.start  = start1;
          this.end    = end1;
          this.diff   = diff1;
        } else {
          this.start  = start2;
          this.end    = end2;
          this.diff   = diff2;
        }

        perform.call(this, mote, shape, emitter_position);
      };

      return this;
    }
  });

  //----------------------------------------------------------------------------------------
  // Custom
  //----------------------------------------------------------------------------------------
  var Custom = Class.extend(Base, {
    initialize: function(func) {
      this.func = func;
    },

    perform: function(mote, shape, position) {
      this.func(mote, shape, position);
    }
  });

  //----------------------------------------------------------------------------------------
  // Color
  //----------------------------------------------------------------------------------------
  var Color = Class.extend(Base, {
    initialize: function(start, end, easing) {
      this.parent(start, end, easing);
    },

    perform: function(mote, shape, emitter_position) {
      this.parent(mote, shape, emitter_position);
      mote.color  = [
        this.start[0] - parseInt(this.diff[0] * this.scale),
        this.start[1] - parseInt(this.diff[1] * this.scale),
        this.start[2] - parseInt(this.diff[2] * this.scale)
      ];
    }
  });

  //----------------------------------------------------------------------------------------
  // Force
  //----------------------------------------------------------------------------------------
  var Force = Class.extend(Base, {
    initialize: function(start, end, easing) {
      this.parent(start, end, easing);
    },

    perform: function(mote, shape, emitter_position) {
      this.parent(mote, shape, emitter_position);
      mote.a[0] += this.start[0] - this.diff[0] * this.scale;
      mote.a[1] += this.start[1] - this.diff[1] * this.scale;
    }
  });

  //----------------------------------------------------------------------------------------
  // Opacity
  //----------------------------------------------------------------------------------------
  var Opacity = Class.extend(Base, {
    initialize: function(start, end, easing) {
      this.parent(start, end, easing);
    },

    perform: function(mote, shape, emitter_position) {
      this.parent(mote, shape, emitter_position);
      mote.opacity = this.start - this.diff * this.scale;
    }
  });

  //----------------------------------------------------------------------------------------
  // Size
  //----------------------------------------------------------------------------------------
  var Size = Class.extend(Base, {
    initialize: function(start, end, easing) {
      this.parent(start, end, easing);
    },

    perform: function(mote, shape, emitter_position) {
      this.parent(mote, shape, emitter_position);
      mote.width  = this.start[0] - this.diff[0] * this.scale;
      mote.height = this.start[1] - this.diff[1] * this.scale;
    }
  });

  return {
    Custom:   Custom,
    Color:    Color,
    Force:    Force,
    Opacity:  Opacity,
    Size:     Size
  };

}));
