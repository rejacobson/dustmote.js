(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Dustmote.Renderer = factory();
    }
}(this, function () {

  var DomRenderer = function(containerElement) {
    this.element  = containerElement;
  };

  DomRenderer.prototype = {
    render: function(mote) {
      if (!mote) return;

      if (mote instanceof Array) {
        for (var i=0, len=mote.length; i<len; i++) {
          this.render(mote[i]);
        }
        return;
      }

      if (!mote.transform.canvas) this.create(mote);

      if (mote.dead) {
        mote.transform.canvas.style.display = 'none';
        return;
      }

      var canvas  = mote.transform.canvas;
      var ctx     = mote.transform.context;

      DomRenderer.transformDom(canvas, mote.p[0] - mote.width/2, mote.p[1] - mote.height/2, 1, 0);

      canvas.style.opacity = mote.opacity;
      canvas.style.display = '';

      canvas.style.width  = mote.width +'px';
      canvas.style.height = mote.height +'px';

      ctx.fillStyle = 'rgb('+ mote.color.join(',') +')';
      ctx.fill();
    },

    create: function(mote) {
      var t = mote.transform;

      t.canvas            = DomRenderer.createCanvas('_'+ mote.id, mote.width, mote.height);
      t.canvas.width      = mote.width + 1;
      t.canvas.height     = mote.height + 1;

      t.context           = t.canvas.getContext('2d');
      t.context.fillStyle = 'rgb('+ mote.color.join(',') +')';

/*
      t.context.beginPath();
      t.context.arc(mote.width/2, mote.height/2, mote.width/2, 0, Math.PI * 2, true);
      t.context.closePath();
*/
      t.context.rect(0, 0, mote.width, mote.height);

      t.context.fill();

      this.element.appendChild(t.canvas);
    }
  };

  DomRenderer.createCanvas = function(id, width, height) {
    var element = document.createElement("canvas");
    element.id = id;
    element.width = width;
    element.height = height;
    element.style.position = 'absolute';
    element.style.opacity = 0;
    this.transformDom(element, -500, -500, 0, 0);
    return element;
  };

  DomRenderer.transformDom = function(el, x, y, scale, rotate) {
    el.style.WebkitTransform = 'translate(' + x + 'px, ' + y + 'px) ' + 'scale(' + scale + ') ' + 'rotate(' + rotate + 'deg)';
    el.style.MozTransform = 'translate(' + x + 'px, ' + y + 'px) ' + 'scale(' + scale + ') ' + 'rotate(' + rotate + 'deg)';
    el.style.OTransform = 'translate(' + x + 'px, ' + y + 'px) ' + 'scale(' + scale + ') ' + 'rotate(' + rotate + 'deg)';
    el.style.msTransform = 'translate(' + x + 'px, ' + y + 'px) ' + 'scale(' + scale + ') ' + 'rotate(' + rotate + 'deg)';
    el.style.transform = 'translate(' + x + 'px, ' + y + 'px) ' + 'scale(' + scale + ') ' + 'rotate(' + rotate + 'deg)';
  };

  var CanvasRenderer = function(canvas) {
    this.canvas = canvas;
  };

  CanvasRenderer.prototype = {
    render: function(particle) {
    }
  };

  return {
    Dom:    DomRenderer,
    Canvas: CanvasRenderer
  };

}));
