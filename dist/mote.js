(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['vector2d'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require['vector2d']);
    } else {
        // Browser globals (root is window)
        root.Dustmote.Mote = factory(root.Vector2d);
    }
}(this, function (V) {

  var _id = 0;

  var Mote = function() {
    this.id = _id++;
    this.reset();
  };

  Mote.prototype = {
    reset: function() {
      this.p  = [0, 0];
      this.v  = [0, 0];
      this.a  = [0, 0];

      this.old = {
        p: [0, 0],
        v: [0, 0],
        a: [0, 0]
      };

      this.age        = 0;
      this.life       = 0;
      this.dead       = false;

      this.mass       = 1.0;
      this.size       = 1.0;
      this.opacity    = 1.0;
      this.radius     = 2;
      this.width      = 5;
      this.height     = 5;
      this.color      = [0, 0, 0];

      if (!this.transform) this.transform = {};

      return this;
    },

    update: function(dt) {
      this.age += dt;

      if (this.age >= this.life) {
        this.dead = true;
      }

      V.set(this.old.p, this.p);
      V.set(this.old.v, this.v);
      V.set(this.old.a, this.a);

      var a = V.scale(V.clone(this.a), dt);
      V.add(this.v, a);

      var v = V.scale(V.clone(this.v), dt);
      V.add(this.p, v);

      V.set(this.a, [0, 0]);
    }
  };

  return Mote;

}));
