(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['vector2d', 'dustmote/util'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require['vector2d', 'dustmote/util']);
    } else {
        // Browser globals (root is window)
        root.Dustmote.Shape = factory(root.Vector2d, root.Dustmote.Util);
    }
}(this, function (V, Util) {

  // --------------------------------------------- //
  var Circle = function(r) {
    this.r = r;
  };

  Circle.prototype = {
    next: function(center) {
      var x = Util.randomRange(-this.r, this.r);
      var y = Math.sqrt(this.r*this.r - x*x);
      y = Util.randomRange(-y, y);
      return [center[0] + x, center[1] + y];
    }
  };

  // --------------------------------------------- //
  var Line = function(endpoint) {
    this.endpoint = endpoint;
  };

  Line.prototype = {
    next: function(center) {
      var dx  = this.endpoint[0] - center[0];
      var dy  = this.endpoint[1] - center[1];
      var m   = dx / dy;
      var x, y;
      if (dx > dy) {
        x = Util.randomRange(center[0], this.endpoint[0]);
        y = x / m;
      } else {
        y = Util.randomRange(center[1], this.endpoint[1]);
        x = y * m;
      }
      return [x, y];
    }
  };

  // --------------------------------------------- //
  var Rect = function(w, h) {
    this.w = w;
    this.h = h;
  };

  Rect.prototype = {
    next: function(center) {
      var x = Util.randomRange(center[0] - this.w/2, center[0] + this.w/2);
      var y = Util.randomRange(center[1] - this.h/2, center[1] + this.h/2);
      return [x, y];
    }
  };

  // --------------------------------------------- //
  var Point = function() {};

  Point.prototype = {
    next: function(center) {
      return V.clone(center);
    }
  };

  return {
    Circle:   Circle,
    Line:     Line,
    Rect:     Rect,
    Point:    Point
  };

}));

