(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['dustmote/mote'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('dustmote/mote'));
    } else {
        // Browser globals (root is window)
        root.Dustmote.Cloud = factory(root.Dustmote.Mote);
    }
}(this, function (Mote) {

  var Cloud = function(max) {
    this.motes = [];
    max = max || 1000;

    for (var i=0; i<max; i++) {
      this.insert(this.create());
    }
  };

  Cloud.prototype = {
    create: function() {
      return new Mote();
    },

    get: function(number) {
      return this.motes.length > 0
        ? this.motes.pop().reset()
        : null; //this.create();
    },

    insert: function(mote) {
      this.motes.push(mote);
      return mote;
    }
  };

  return Cloud;

}));
