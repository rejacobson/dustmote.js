(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Dustmote.RangedValue = factory();
    }
}(this, function () {

  //  a                          b
  //  ----------------------------
  //
  //
  // a-c           a            a+b
  //  ----------------------------
  var RangedValue = function(a, b, c) {
    this.a = a != undefined ? a : 1;
    this.b = b != undefined ? b : this.a;
    this.c = c;

    this.isArray = Array.isArray(a);
  };

  RangedValue.prototype = {
    getValue: function() {
      if (this.isArray) {
        return this.a[Math.floor(this.a.length * Math.random())];
      } else {
        return !this.c
          ? Dustmote.Util.randomRange(this.a, this.b)
          : Dustmote.Util.randomRange(this.a + this.b, this.a - this.c);
      }
    }
  };

  return RangedValue;

}));
