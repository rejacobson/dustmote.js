(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['vector2d', 'dustmote/shape'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('vector2d'), require('dustmote/shape'));
    } else {
        // Browser globals (root is window)
        root.Dustmote.Emitter = factory(root.Vector2d, root.Dustmote.Shape);
    }
}(this, function (V, Shape) {

  var Emitter = function(motes_per_sec, position, shape) {
    this.rate       = 1/motes_per_sec;
    this.timer      = 0;

    this.position   = position || [0, 0];
    this.shape      = shape || new Shape.Point();

    this.motes      = [];
    this.dead       = [];

    this.styletools = [];
    this.forgetools = [];

    this.dustmote;

    this.free_indexes = [];
  };

  Emitter.prototype = {
    produceMotes: function(dt) {
      var num_to_forge;
      var mote;

      this.timer += dt;

      num_to_forge = Math.floor(this.timer/this.rate);

      if (num_to_forge > 0) this.timer = this.timer % this.rate;

      while (num_to_forge--) {
        mote = this.dustmote.dustcloud.get();
        if (mote) this.forge(mote);
      }
    },

    removeDeadMotes: function() {
      if (!this.dead.length) return;

      var mote;

      while (mote = this.dead.pop()) {
        this.motes[mote._index] = undefined;
        this.free_indexes.push(mote._index);
        mote._index = undefined;
        this.dustmote.dustcloud.insert(mote);
      }
    },

    update: function(dt) {
      var mote;

      this.produceMotes(dt);

      this.removeDeadMotes();

      for (var i=0, ilen=this.motes.length; i<ilen; i++) {
        mote = this.motes[i];

        if (!mote) continue;

        if (mote.dead) {
          this.dead.push(mote);
          continue;
        }

        for (var j=0, jlen=this.styletools.length; j<jlen; j++) {
          this.styletools[j].perform(mote, dt);
        }

        mote.update(dt);
      }
    },

    forge: function(mote) {
      if (!mote) return;

      mote.p = this.shape.next(this.position);

      for (var i=0, len=this.forgetools.length; i<len; i++) {
        this.forgetools[i].perform(mote);
      }

      mote._index = this.free_indexes.length
        ? this.free_indexes.pop()
        : this.motes.length;

      this.motes[mote._index] = mote;
    },

    styleWith: function(tool) {
      this.styletools.push(tool);
    },

    forgeWith: function(tool) {
      this.forgetools.push(tool);
    }
  };

  return Emitter;

}));
