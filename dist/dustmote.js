(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['vector2d'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('vector2d'));
    } else {
        // Browser globals (root is window)
        root.Dustmote = factory(root.Vector2d);
    }
}(this, function (V) {

  var Dustmote = function(dustcloud, renderer) {
    this.emitters   = [];
    this.renderer   = renderer;
    this.dustcloud  = dustcloud;
  };

  Dustmote.prototype = {
    update: function(dt) {
      for (var i=0, len=this.emitters.length; i<len; i++) {
        this.emitters[i].update(dt);
      }
    },

    render: function() {
      for (var i=0, len=this.emitters.length; i<len; i++) {
        this.renderer.render(this.emitters[i].motes);
      }
    },

    addEmitter: function(emitter) {
      emitter.dustmote = this;
      this.emitters.push(emitter);
    }
  };

  return Dustmote;

}));
