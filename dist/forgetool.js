(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['inheritance', 'dustcloud/rangedvalue'], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('inheritance'), require('dustcloud/rangedvalue'));
    } else {
        // Browser globals (root is window)
        root.Dustmote.ForgeTool = factory(root.Class, root.Dustmote.RangedValue);
    }
}(this, function (Class, RangedValue) {

  var Base = Class.extend({
    initialize: function(a, b, c) {

    },

    perform: function(mote, shape, emitter_position) {

    }
  });

  var Life = function(a, b, c) {
    this.life = new RangedValue(a, b, c);
  };

  Life.prototype = {
    perform: function(mote, shape, position) {
      mote.life = this.life.getValue();
    }
  };

  var Velocity = function(a, b, c) {
    this.velocity_x = new RangedValue(
      a[0] != undefined ? a[0] : a,
      b != undefined && b[0] != undefined ? b[0] : b,
      c != undefined && c[0] != undefined ? c[0] : c
    );

    this.velocity_y = new RangedValue(
      a[1] != undefined ? a[1] : a,
      b != undefined && b[1] != undefined ? b[1] : b,
      c != undefined && c[1] != undefined ? c[1] : c
    );
  };

  Velocity.prototype = {
    perform: function(mote, shape, position) {
      mote.v[0] = this.velocity_x.getValue();
      mote.v[1] = this.velocity_y.getValue();
    }
  };

  return {
    Life:     Life,
    Velocity: Velocity
  };

}));
