(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Dustmote.Util = factory();
    }
}(this, function () {

  return {
    randomRange: function(a, b) {
      return a + Math.random() * (b - a);
    },

    interleave: function(a, b, callback) {
      if (!b) return;
      if (this.isArray(a) && a.length > (b.length || 0)) {
        var last_b = b;
        for (var i=0, len=a.length; i<len; i++) {
          if (b[i] != undefined && b[i] != null) last_b = b[i];
          callback(a[i], last_b);
        }
      } else if (this.isArray(b)) {
        var last_a = a;
        for (var i=0, len=b.length; i<len; i++) {
          if (a[i] != undefined && a[i] != null) last_a = a[i];
          callback(last_a, b[i]);
        }
      } else {
        callback(a, b);
      }
    },

    isArray: function(object) {
      return object instanceof Array;
    },

    isFunction: function(object) {
      return typeof(object) === 'function';
    },

    isEven: function(num) {
      return Math.floor(num) % 2 == 0;
    }
  };

}));
